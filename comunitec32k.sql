-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2020 a las 20:31:56
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `comunitec32k`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec32k_cursos`
--

CREATE TABLE `comunitec32k_cursos` (
  `idCurso` int(11) NOT NULL,
  `nombre_curso` varchar(50) NOT NULL,
  `sub_titulo_curso` varchar(100) NOT NULL,
  `descripcion_curso` varchar(535) NOT NULL,
  `duracion_curso_n` tinyint(13) NOT NULL,
  `duracion_curso_p` varchar(20) NOT NULL,
  `inicio_curso` date NOT NULL,
  `cierre_curso` date NOT NULL,
  `nivel_curso` varchar(30) NOT NULL,
  `certificacion` varchar(3) NOT NULL,
  `Imagen` text NOT NULL,
  `IsActive` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comunitec32k_cursos`
--

INSERT INTO `comunitec32k_cursos` (`idCurso`, `nombre_curso`, `sub_titulo_curso`, `descripcion_curso`, `duracion_curso_n`, `duracion_curso_p`, `inicio_curso`, `cierre_curso`, `nivel_curso`, `certificacion`, `Imagen`, `IsActive`) VALUES
(8, 'Programacion ', 'Aquí aprendes a programar', 'Este curso se va a conocer PHP, HTML y Python ', 3, '0', '2020-06-01', '2020-09-01', 'Basico', 'Si', '', 1),
(9, 'Programacion Intermedia', 'Programa un poco mejor con este curso', 'En este curso se aprendera la POO en PHP', 2, '0', '2020-06-27', '2020-08-27', 'Intermedio', 'Si', '', 1),
(10, 'Programacion Avanzada', 'Programa cualquier cosa que te pase por tu mente ', 'En este ultimo nivel de programación lo que puedes imaginar lo puedes programar ', 1, 'Semanas', '2020-08-03', '2020-08-23', 'Unico', 'Si', '', 0),
(11, 'Primaria gratuita ', 'Es gratis', 'Aprende aqui', 3, '0', '2020-06-16', '2020-09-16', 'Unico', 'Si', '', 1),
(14, 'Prueba', 'Esto es una Prueba', '123qwe', 1, '0', '2020-06-01', '2020-06-07', 'Unico', 'Si', '', 1),
(15, 'qwe', 'ad', 'asd', 2, 'Semanas', '2020-06-01', '2020-06-07', 'Unico', 'Si', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec32k_eventos`
--

CREATE TABLE `comunitec32k_eventos` (
  `id_evento` int(11) NOT NULL,
  `nombre_evento` varchar(30) NOT NULL,
  `sub_titulo_evento` varchar(100) NOT NULL,
  `descripcion_evento` varchar(255) NOT NULL,
  `duracion_evento` varchar(255) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_cierre` date NOT NULL,
  `hora_inicio_evento_h` varchar(10) NOT NULL,
  `hora_inicio_evento_p` varchar(10) NOT NULL,
  `PO_imparten` varchar(255) NOT NULL,
  `isActive` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comunitec32k_eventos`
--

INSERT INTO `comunitec32k_eventos` (`id_evento`, `nombre_evento`, `sub_titulo_evento`, `descripcion_evento`, `duracion_evento`, `fecha_inicio`, `fecha_cierre`, `hora_inicio_evento_h`, `hora_inicio_evento_p`, `PO_imparten`, `isActive`) VALUES
(6, 'Musica', 'Escucha música ', 'Buena musica', '4:30 hrs', '2020-06-01', '2020-06-01', '8:30', 'AM', 'Orquesta falsa ', b'1'),
(8, 'prueba', 'Esto es una prueba', 'qweasd', '1:00 hr', '2020-06-01', '2020-06-07', '1:00', 'AM', 'Orquesta falsa 2', b'0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec32k_tst`
--

CREATE TABLE `comunitec32k_tst` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_bitacora`
--

CREATE TABLE `comunitec_tbl_bitacora` (
  `id_entrada` int(11) NOT NULL,
  `id_usuario` smallint(6) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_estudiantes`
--

CREATE TABLE `comunitec_tbl_estudiantes` (
  `id_registro` int(11) NOT NULL,
  `nombre_alumno` varchar(30) NOT NULL,
  `ap_paterno_alumno` varchar(30) NOT NULL,
  `ap_materno_alumno` varchar(30) NOT NULL,
  `curso_inscrito` varchar(100) NOT NULL,
  `edad_alumno` int(11) NOT NULL,
  `correo_alumno` varchar(50) NOT NULL,
  `nombre_calle_alumno` varchar(30) NOT NULL,
  `numero_domicilio` int(11) NOT NULL,
  `numero_domicilio_interno` varchar(11) NOT NULL,
  `colonia_fraccionamiento` varchar(50) NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `municipio_residente` varchar(50) NOT NULL,
  `estado_residente` varchar(50) NOT NULL,
  `genero_alumno` varchar(10) NOT NULL,
  `f_nacimiento_alumno` date NOT NULL,
  `pais_nacimiento` varchar(50) NOT NULL,
  `estado_nacimiento` varchar(50) NOT NULL,
  `ciudad_nacimiento` varchar(50) NOT NULL,
  `CURP` varchar(18) NOT NULL,
  `celular` varchar(10) NOT NULL,
  `telefono_emergencia` varchar(10) NOT NULL,
  `estado_civil` varchar(50) NOT NULL,
  `ultimo_grado_estudios` varchar(50) NOT NULL,
  `situacion_actual` varchar(50) NOT NULL,
  `isActive` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_instructores`
--

CREATE TABLE `comunitec_tbl_instructores` (
  `id_instructor` int(11) NOT NULL,
  `nombre_ins` varchar(30) NOT NULL,
  `apellido_ins` varchar(30) NOT NULL,
  `correo_ins` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `isActive` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comunitec_tbl_instructores`
--

INSERT INTO `comunitec_tbl_instructores` (`id_instructor`, `nombre_ins`, `apellido_ins`, `correo_ins`, `telefono`, `isActive`) VALUES
(1, 'Andres', 'Nuñez', 'profeAndres@hotmail.com', '123321', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_motivos_de_visita`
--

CREATE TABLE `comunitec_tbl_motivos_de_visita` (
  `id_motivos_de_visita` tinyint(4) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `campo_valido` bit(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comunitec_tbl_motivos_de_visita`
--

INSERT INTO `comunitec_tbl_motivos_de_visita` (`id_motivos_de_visita`, `descripcion`, `campo_valido`) VALUES
(1, 'Computación básica', b'1'),
(2, 'Computación avanzada ', b'1'),
(3, 'Programación', b'1'),
(4, 'Diseño gráfico ', b'1'),
(5, 'Diseño industrial', b'1'),
(6, 'Ingles', b'1'),
(7, 'Primaria abierta', b'1'),
(8, 'Secundaria abierta', b'1'),
(9, 'Preparatoria abierta', b'1'),
(10, 'Un pedazo de pie', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_registro_visitas`
--

CREATE TABLE `comunitec_tbl_registro_visitas` (
  `no_de_visita` smallint(6) NOT NULL,
  `correo_telefono` varchar(50) NOT NULL,
  `fecha_solicito_visita` timestamp NOT NULL DEFAULT current_timestamp(),
  `descripcion` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comunitec_tbl_registro_visitas`
--

INSERT INTO `comunitec_tbl_registro_visitas` (`no_de_visita`, `correo_telefono`, `fecha_solicito_visita`, `descripcion`) VALUES
(11, 'Joshua', '2020-06-03 02:16:21', 'Otro'),
(12, 'alguien@hotmail.com', '2020-06-17 14:17:51', 'Computación '),
(13, 'alguien@hotmail.com', '2020-07-03 01:13:05', 'Diseño gráfico '),
(14, 'alguien@hotmail.com', '2020-07-02 17:13:42', 'Preparatoria abierta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_tareas`
--

CREATE TABLE `comunitec_tbl_tareas` (
  `is_tarea` int(11) NOT NULL,
  `id_clase` int(11) NOT NULL,
  `id_ins` int(11) NOT NULL,
  `nombre_ins` int(11) NOT NULL,
  `decripcion_tarea` text NOT NULL,
  `fecha_encargada` datetime NOT NULL,
  `fecha_entrega` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_tipos_de_usuario`
--

CREATE TABLE `comunitec_tbl_tipos_de_usuario` (
  `id_tipo_usuario` tinyint(4) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comunitec_tbl_tipos_de_usuario`
--

INSERT INTO `comunitec_tbl_tipos_de_usuario` (`id_tipo_usuario`, `descripcion`) VALUES
(1, 'Admin '),
(2, 'Usuario comunitec ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tbl_usuarios`
--

CREATE TABLE `comunitec_tbl_usuarios` (
  `id_usuario` smallint(6) NOT NULL,
  `ap_Paterno` varchar(20) DEFAULT NULL,
  `ap_Materno` varchar(20) DEFAULT NULL,
  `nombre` varchar(20) NOT NULL,
  `ano_nacimiento` mediumint(4) DEFAULT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `organizacion` varchar(20) DEFAULT NULL,
  `colonia` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comunitec_tbl_usuarios`
--

INSERT INTO `comunitec_tbl_usuarios` (`id_usuario`, `ap_Paterno`, `ap_Materno`, `nombre`, `ano_nacimiento`, `correo_electronico`, `telefono`, `organizacion`, `colonia`) VALUES
(3, 'Saenz', 'Espinoza', 'Joshua', 2001, 'el@hotmail.com', '6561233214', 'casa', 'Ex hipodromo'),
(4, 'Terrazas', 'Espinoza', 'Emilio', 2008, 'tu@hotmail.com', '6561231234', 'casa', 'Ex Hipodromo'),
(7, 'Saenz', 'Espinoza', 'Joshua', 2001, 'prueba@hotmail.com', '7897897897', 'Casa', 'Ex hipodromo'),
(8, 'as', 'Espinoza', 'Joshua', 2001, 'alguien@hotmail.com', '7897897897', 'Casa', 'Ex hipodromo'),
(9, 'as', 'Espinoza', 'Joshua', 2004, 'alguien@hotmail.com', '7897897897', 'Casa', 'Ex hipodromo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunitec_tst_tbl_usuarios`
--

CREATE TABLE `comunitec_tst_tbl_usuarios` (
  `id_usuario` smallint(6) NOT NULL,
  `ap_Paterno` varchar(30) NOT NULL,
  `ap_Materno` varchar(30) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `f_nacimiento` date NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `organizacion` varchar(20) NOT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `id_tipo_usuario` tinyint(1) NOT NULL,
  `usuario_activo` bit(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comunitec_tst_tbl_usuarios`
--

INSERT INTO `comunitec_tst_tbl_usuarios` (`id_usuario`, `ap_Paterno`, `ap_Materno`, `nombre`, `f_nacimiento`, `correo_electronico`, `contrasena`, `telefono`, `organizacion`, `colonia`, `id_tipo_usuario`, `usuario_activo`) VALUES
(25, 'Saenz', 'Espinoza', 'Joshua', '2020-06-01', 'ADMIN@hotmail.com', '321', '6562993305', 'Casa', 'Ex hipodromo', 1, b'1'),
(26, 'Terrazas', 'Espinoza', 'Emilio', '2020-06-02', 'USUARIO@hotmail.com', '321', '6567654321', 'casa', 'Ex Hipodromo', 2, b'1'),
(27, 'Saenz', 'Espinoza', 'Joshua', '2020-06-02', 'alguien@hotmail.com', '321', '6561234567', 'Casa', 'Ex hipodromo', 2, b'1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comunitec32k_cursos`
--
ALTER TABLE `comunitec32k_cursos`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indices de la tabla `comunitec32k_eventos`
--
ALTER TABLE `comunitec32k_eventos`
  ADD PRIMARY KEY (`id_evento`);

--
-- Indices de la tabla `comunitec32k_tst`
--
ALTER TABLE `comunitec32k_tst`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comunitec_tbl_bitacora`
--
ALTER TABLE `comunitec_tbl_bitacora`
  ADD PRIMARY KEY (`id_entrada`);

--
-- Indices de la tabla `comunitec_tbl_estudiantes`
--
ALTER TABLE `comunitec_tbl_estudiantes`
  ADD PRIMARY KEY (`id_registro`);

--
-- Indices de la tabla `comunitec_tbl_instructores`
--
ALTER TABLE `comunitec_tbl_instructores`
  ADD PRIMARY KEY (`id_instructor`);

--
-- Indices de la tabla `comunitec_tbl_motivos_de_visita`
--
ALTER TABLE `comunitec_tbl_motivos_de_visita`
  ADD PRIMARY KEY (`id_motivos_de_visita`);

--
-- Indices de la tabla `comunitec_tbl_registro_visitas`
--
ALTER TABLE `comunitec_tbl_registro_visitas`
  ADD PRIMARY KEY (`no_de_visita`);

--
-- Indices de la tabla `comunitec_tbl_tareas`
--
ALTER TABLE `comunitec_tbl_tareas`
  ADD PRIMARY KEY (`is_tarea`);

--
-- Indices de la tabla `comunitec_tbl_tipos_de_usuario`
--
ALTER TABLE `comunitec_tbl_tipos_de_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `comunitec_tbl_usuarios`
--
ALTER TABLE `comunitec_tbl_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `comunitec_tst_tbl_usuarios`
--
ALTER TABLE `comunitec_tst_tbl_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comunitec32k_cursos`
--
ALTER TABLE `comunitec32k_cursos`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `comunitec32k_eventos`
--
ALTER TABLE `comunitec32k_eventos`
  MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `comunitec32k_tst`
--
ALTER TABLE `comunitec32k_tst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_bitacora`
--
ALTER TABLE `comunitec_tbl_bitacora`
  MODIFY `id_entrada` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_estudiantes`
--
ALTER TABLE `comunitec_tbl_estudiantes`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_instructores`
--
ALTER TABLE `comunitec_tbl_instructores`
  MODIFY `id_instructor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_motivos_de_visita`
--
ALTER TABLE `comunitec_tbl_motivos_de_visita`
  MODIFY `id_motivos_de_visita` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_registro_visitas`
--
ALTER TABLE `comunitec_tbl_registro_visitas`
  MODIFY `no_de_visita` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_tareas`
--
ALTER TABLE `comunitec_tbl_tareas`
  MODIFY `is_tarea` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_tipos_de_usuario`
--
ALTER TABLE `comunitec_tbl_tipos_de_usuario`
  MODIFY `id_tipo_usuario` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `comunitec_tbl_usuarios`
--
ALTER TABLE `comunitec_tbl_usuarios`
  MODIFY `id_usuario` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `comunitec_tst_tbl_usuarios`
--
ALTER TABLE `comunitec_tst_tbl_usuarios`
  MODIFY `id_usuario` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
