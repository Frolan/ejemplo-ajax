<?php
require_once "connection.php";

session_start();

if(isset($_SESSION['error'] )){
	$failure = $_SESSION['error'];
}


if(isset($_POST['acceder'])){
	session_destroy();
	session_start();
	$correo = trim($_POST['email']);
	$contraseña = trim($_POST['contraseña']);
	
		$qry = "SELECT * FROM comunitec_tst_tbl_usuarios
					WHERE correo_electronico = :correo AND contrasena = :password";
					
		try{
			$stm = $link->prepare($qry);
			$stm->execute(array(
					':correo' => $correo,
					':password' => $contraseña)
			);
			$row = $stm->fetch(PDO::FETCH_ASSOC);
			
			if(isset($row)){
				if($row['id_usuario']>0){
					$_SESSION['entrada'] = 1;
					
					$_SESSION['id_usuario'] = htmlentities($row['id_usuario']);
					$_SESSION['nombre'] = htmlentities($row['nombre']);
					$_SESSION['ap_Paterno'] = htmlentities($row['ap_Paterno']);
					$_SESSION['ap_Materno'] = htmlentities($row['ap_Materno']);
					$_SESSION['f_nacimiento'] = htmlentities($row['f_nacimiento']);
					$_SESSION['correo_electronico'] = htmlentities($row['correo_electronico']);
					$_SESSION['contrasena'] = htmlentities($row['contrasena']);
					$_SESSION['telefono'] = htmlentities($row['telefono']);
					$_SESSION['organizacion'] = htmlentities($row['organizacion']);
					$_SESSION['colonia'] = htmlentities($row['colonia']);
					$_SESSION['id_tipo_usuario'] = htmlentities($row['id_tipo_usuario']);
					$_SESSION['usuario_activo'] = htmlentities($row['usuario_activo']);
					
					header('Location: index.php');
					return;
			
				}else{
						$_SESSION['error'] = "El usuario y/o contraseña estan mal";
				}
			}	
		}	//Fin del try			
		catch(Exception $ex){
				echo '<h3> Hubo un error, favor de contactar con el soporte tecnio</h3>';
				die("Error con la busqueda de usuario: " . $e->GetMessage());
		}				
}
?>

<html>
<head>

  <meta charset="utf-8">
  <title>Practia Ajax</title>

</head>
<body>

    <div class="container">
				<h1>Inicio de sesión</h1>
				<?php
					if( isset($_SESSION['error']) ){
						echo '<p style="color:red;">'.htmlentities($_SESSION['error']).'</p>';
						unset($_SESSION['error']);		
					}
				?>
              <form method="POST" role="form" class="php-email-form">
			  
			    <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required data-rule="email" data-msg="Porfavor, ingresa un correo valido" />
                  <div class="validate"></div>
                </div>
				
                <div class="form-group">
                  <input type="password" name="contraseña" class="form-control" id="name" placeholder="Contraseña" required />
                  <div class="validate"></div>
                </div>
				
                <div class="text-center">
					<input type="submit" name="acceder" Value="Acceder">
					<input type="reset" name="reset" Value="Restablecer" class="btn_submit" >
				</div>
              </form>
			  
			  
	</div>	
	 
</body>
</html>